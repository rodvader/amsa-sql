'use strict'

const utils = require('../utils')
const config = require('../../config')
const sql = require('mssql')

const getEvents = async () => {
    try {
        let pool = await sql.connect(config.sql)
        const sqlQueries = await utils.loadSqlQueries('events')
        const list = await pool.request().query(sqlQueries.eventList)
        return list.recordset
    } catch (e) {
        return e.message
    }
}

module.exports = { getEvents }