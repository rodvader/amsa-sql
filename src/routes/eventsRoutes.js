'use strict'

const express = require('express')
const eventController = require('../controllers/eventsController')
const router = express.Router()

const {getEvents} = eventController

router.get('/', getEvents)

module.exports = {routes: router}