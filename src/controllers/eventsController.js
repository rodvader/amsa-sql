'use strict'

const eventData = require('../db/events')

const getEvents = async (req, res, next) =>{
    try {
        const events = await eventData.getEvents();
        console.log(events)
        res.send(events)
    } catch (e){
        res.status(400).send(e.message)
    }
}

module.exports = {getEvents}