'use strict'
const express = require('express')
const config = require('./config')
const cors = require('cors')
const bodyParser = require('body-parser')

const eventRoutes = require('./routes/eventsRoutes')


const app = express()

//Use
app.use(cors())
app.use(bodyParser.json())

app.use('/', eventRoutes.routes)

//Middleware



app.listen(config.port, () => {
    console.log(`Servidor iniciado en el puerto  ${config.port}:  http://localhost:${config.port}`)
})

