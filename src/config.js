'use strict';

const dotenv = require('dotenv')
const assert = require('assert')

dotenv.config()


const { PORT, HOST, HOST_URL, SQL_USER, SQL_PASSWORD, SQL_SERVER, SQL_DATABASE, SQL_ENCRYPT } = process.env

const sqlEcrypt = process.env.SQL_ENCRYPT === "true"

assert(PORT, 'PORT is Required')
assert(HOST, 'HOST is Required')

module.exports = {
    port: PORT,
    host: HOST,
    url: HOST_URL,
    sql: {
        server: SQL_SERVER,
        user: SQL_USER,
        password: SQL_PASSWORD,
        options:{
            encript: sqlEcrypt,
            enableArithAbort: true
        }

    }
}